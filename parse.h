
#ifndef HOMEWORK7_PARSE_H
#define HOMEWORK7_PARSE_H

#include "header.h"
#include <stdint.h>

/**
 * Reads an IP packet header.
 *
 * @param packet The bytes of the packet.
 * @param packet_size The size of the input packet.
 * @return NULL if the header size is too small, otherwise a pointer to a heap
 * allocated Header struct.
 */
Header *read_header(unsigned char packet[], uint32_t packet_size);

#endif // HOMEWORK7_PARSE_H
