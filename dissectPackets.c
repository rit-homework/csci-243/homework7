//
// Created by mxc4400 on 4/4/18.
//

#define _DEFAULT_SOURCE
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <zconf.h>

#include "header.h"
#include "parse.h"
#include "print.h"

#define MAX_PACKET_SIZE 2048

/**
 * The main method, opens file, reads and prints packets.
 */
int main(int argc, char *argv[]) {
    // Get File Argument
    if (argc != 2) {
        fprintf(stderr, "usage: dissectPackets inputFile\n");
        return 1;
    }

    char *file_name = argv[1];

    // Open file
    FILE *file = fopen(file_name, "rb");
    if (file == NULL) {
        perror("Failed to open input file");
        return 1;
    }

    // Read Number of Packets
    uint32_t raw_packet_count = 0;
    size_t bytes_read =
        fread(&raw_packet_count, sizeof(raw_packet_count), 1, file);
    if (bytes_read == 0) {
        fprintf(stderr, "Failed to read count of packets.\n");
        int res = fclose(file);
        if (res) {
            perror("dissectPackets close");
        }
        return 1;
    }

    uint32_t packet_count = le32toh(raw_packet_count);
    printf("==== File %s contains %d Packets.\n", file_name, packet_count);

    // Read Packets
    for (uint32_t packet_number = 1; packet_number <= packet_count;
         packet_number++) {
        printf("==>Packet %d\n", packet_number);

        // Read Packet Size
        uint32_t packet_size_raw = 0;
        bytes_read = fread(&packet_size_raw, sizeof(packet_size_raw), 1, file);
        if (bytes_read == 0) {
            perror("dissectPackets");
            int res = fclose(file);
            if (res) {
                perror("dissectPackets close");
            }
            return 1;
        }

        uint32_t packet_size = le32toh(packet_size_raw);
        if (packet_size > MAX_PACKET_SIZE) {
            fprintf(stderr,
                    "The packet is greater than the maximum packet size (%d). "
                    "Got %d.\n",
                    MAX_PACKET_SIZE, packet_size);
            int res = fclose(file);
            if (res) {
                perror("dissectPackets close");
            }
            return 1;
        }

        // Read Packet
        unsigned char packet[packet_size];
        bytes_read = fread(&packet, sizeof(packet), 1, file);
        if (bytes_read == 0) {
            perror("dissectPackets");
            int res = fclose(file);
            if (res) {
                perror("dissectPackets close");
            }
            return 1;
        }

        // Parse Packet
        Header *header = read_header(packet, packet_size);
        if (header == NULL) {
            int res = fclose(file);
            if (res) {
                perror("dissectPackets close");
            }
            return 1;
        }
        print_header(header);
        free(header);
    }

    int res = fclose(file);
    if (res) {
        perror("dissectPackets close");
    }
}
