//
// Created by mxc4400 on 4/4/18.
//

#ifndef HOMEWORK7_PRINT_H
#define HOMEWORK7_PRINT_H
#include "header.h"
#include <stdint.h>

/**
 * Prints the header in the format specified in the assignment.
 *
 * @param header The header to print to standard output.
 */
void print_header(Header *header);

#endif // HOMEWORK7_PRINT_H
