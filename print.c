//
// Created by mxc4400 on 4/4/18.
//

#include "print.h"
#include <stdio.h>

/**
 * A helper function to print information about a field with a string following
 * the spacing.
 *
 * @param field_name The name of the field.
 * @param following The string which should be printed after the spacint.
 * @param number The value of the field.
 */
static void print_field_with_following(char *field_name, const char *following,
                                       uint32_t number) {
    printf("%s:\t\t%s 0x%x (%d)\n", field_name, following, number, number);
}

/**
 * A helper function to print information about a field.
 *
 * @param field_name The name of the field to print.
 * @param number The value of the field.
 */
static void print_field(char *field_name, uint32_t number) {
    printf("%s:\t\t0x%x (%d)\n", field_name, number, number);
}

/**
 * Gets a synonym to the IP packet protocol identifier.
 *
 * @param id The IP packet protocol identifier.
 * @return A string representation of the identifier.
 */
static const char *synonym(uint8_t id) {
    switch (id) {
    case 1:
        return "ICMP";

    case 2:
        return "IGMP";

    case 6:
        return "TCP";

    case 9:
        return "IGRP";

    case 17:
        return "UDP";

    case 47:
        return "GRE";

    case 50:
        return "ESP";

    case 51:
        return "AH";

    case 57:
        return "SKIP";

    case 88:
        return "EIGRP";

    case 89:
        return "OSPF";

    case 115:
        return "L2TP";

    default:
        return "UNKNOWN";
    }
}

/**
 * A helper function to print an IP address as 4 octets.
 *
 * @param field_name The name of the field to which the address corresponds to.
 * @param address The address to print.
 */
static void print_address(const char *field_name, uint32_t address) {
    printf("%s:\t\t%d.%d.%d.%d\n", field_name, (address >> 8u * 3) & 0xFFu,
           (address >> 8u * 2) & 0xFFu, (address >> 8u * 1) & 0xFFu,
           (address >> 8u * 0) & 0xFFu);
}

void print_header(Header *header) {
    print_field("Version", header->version);
    print_field("IHL (Header Length)", header->ihl);
    print_field("Type of Service (TOS)", header->type_of_service);
    print_field("Total Length", header->total_length);
    print_field("Identification", header->identification);
    print_field("IP Flags", header->flags);
    print_field("Fragment Offset", header->fragment_offset);
    print_field("Time To Live (TTL)", header->time_to_live);
    print_field_with_following("Protocol", synonym(header->protocol),
                               header->protocol);
    print_field("Header Checksum", header->header_checksum);
    print_address("Source Address", header->source_address);
    print_address("Destination Address", header->destination_address);
}
