//
// Created by mxc4400 on 4/4/18.
//

#ifndef HOMEWORK7_HEADER_H
#define HOMEWORK7_HEADER_H
#include <stdint.h>

/**
 * The parsed information from the IP header.
 */
typedef struct {
    /**
     * The Version field indicates the format of the internet header.
     */
    uint8_t version;

    /**
     * Internet Header Length is the length of the internet header in 32 bit
     * words, and thus points to the beginning of the data. Note that the
     * minimum value for a correct header is 5.
     */
    uint8_t ihl;

    /**
     * The Type of Service provides an indication of the abstract parameters of
     * the quality of service desired.  These parameters are to be used to guide
     * the selection of the actual service parameters when transmitting a
     * datagram through a particular network.
     */
    uint8_t type_of_service;

    /**
     * Total Length is the length of the datagram, measured in octets, including
     * internet header and data.
     */
    uint16_t total_length;

    /**
     * An identifying value assigned by the sender to aid in assembling the
     * fragments of a datagram.
     */
    uint16_t identification;

    /**
     * Various Control Flags.
     */
    uint8_t flags;

    /**
     * Fragment offset from start of IP datagram. Measured in 8 byte increments.
     */
    uint16_t fragment_offset;

    /**
     * This field indicates the maximum time the datagram is allowed to remain
     * in the internet system.
     */
    uint8_t time_to_live;

    /**
     *  This field indicates the next level protocol used in the data portion of
     * the internet datagram.
     */
    uint8_t protocol;

    /**
     * A checksum on the header only.
     */
    uint16_t header_checksum;

    /**
     * The source address.
     */
    uint32_t source_address;

    /**
     * The destination address.
     */
    uint32_t destination_address;
} Header;

#endif // HOMEWORK7_HEADER_H
