CFLAGS=-Wall -Wextra -std=c99 -Werror -ggdb -pedantic
VALGRIND=valgrind --error-exitcode=1 --leak-check=full --show-leak-kinds=all

all: dissectPackets

dissectPackets: dissectPackets.o parse.o parse.h print.o print.h header.h
	$(CC) $(CFLAGS) -o $@ $^

test: tests/1

tests/1: dissectPackets
	$(VALGRIND) ./dissectPackets tests/1/packets > tests/1/actual.txt
	diff tests/1/actual.txt tests/1/output.txt

fix-formatting:
	clang-format -i *.c *.h

clean:
	rm *.o dissectPackets

.PHONY: tests/1 clean fix-formatting
