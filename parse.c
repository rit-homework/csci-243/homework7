//
// Created by mxc4400 on 4/4/18.
//

#define _DEFAULT_SOURCE
#include "parse.h"
#include "header.h"
#include <endian.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * A struct to extract structured meaningful information from IP packet headers
 * without doing pointer arithmetic.
 */
typedef struct {
    /**
     * Version and ihl fields. The version takes up the first four bits and the
     * ihl takes up the last four bits.
     */
    uint8_t version_and_ihl;

    /**
     * Byte representing type of service.
     */
    uint8_t type_of_service;

    /**
     * Total length field.
     */
    uint16_t total_length;

    /**
     * Identification field.
     */
    uint16_t identification;

    /**
     * Flags and fragment offset fields.
     */
    uint16_t flags_and_fragment_offset;

    /**
     * Time to live field.
     */
    uint8_t time_to_live;

    /**
     * Protocol identifier byte.
     */
    uint8_t protocol;

    /**
     * Header checksum bytes.
     */
    uint16_t header_checksum;

    /**
     * Source address.
     */
    uint32_t source_address;

    /**
     * Destination address.
     */
    uint32_t destination_address;
} RawHeader;

Header *read_header(unsigned char *packet, uint32_t packet_size) {
    // Unpack Into Raw Header
    if (packet_size < sizeof(RawHeader)) {
        fprintf(stderr, "The packet is too small (%zu). Got %d",
                sizeof(RawHeader), packet_size);
        return NULL;
    }

    RawHeader *raw_header = (RawHeader *)packet;
    Header *header = malloc(sizeof(Header));

    header->version = raw_header->version_and_ihl >> 4u;
    header->ihl = raw_header->version_and_ihl & (uint8_t)(0xFFu >> 4u);
    header->type_of_service = raw_header->type_of_service;
    header->total_length = be16toh(raw_header->total_length);
    header->identification = be16toh(raw_header->identification);

    header->flags =
        (uint8_t)(raw_header->flags_and_fragment_offset & (0xF0u << 1u));

    header->fragment_offset = be16toh(raw_header->flags_and_fragment_offset &
                                      (uint16_t)(0xFFu >> 3u));
    header->time_to_live = raw_header->time_to_live;
    header->protocol = raw_header->protocol;
    header->header_checksum = be16toh(raw_header->header_checksum);
    header->source_address = be32toh(raw_header->source_address);
    header->destination_address = be32toh(raw_header->destination_address);

    return header;
}
